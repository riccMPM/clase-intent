package com.pe.navegacion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button butEnviar;
    EditText eteNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eteNombre = findViewById(R.id.eteNombre);
        butEnviar = findViewById(R.id.butEnviar);
        butEnviar.setOnClickListener(this);

        //Wasaaaaaaa

    }


    @Override
    public void onClick(View view) {
        Toast.makeText(this, "Click boton enviar", Toast.LENGTH_SHORT).show();

        Pedro pedro = new Pedro();
        pedro.habilidades = "Trabajador";

        String valor = eteNombre.getText().toString();

        //Intent
        Intent intent = new Intent(this, DestinoActivity.class);
        intent.putExtra("nombre", "Riccardo Mija");
        intent.putExtra("edad", 27);
        intent.putExtra("objeto", pedro);
        intent.putExtra("wewesa", valor);

        startActivity(intent);
    }
}
